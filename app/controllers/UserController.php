<?php

class UserController extends BaseController
{
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        $this->beforeFilter('role');
    }

    /* get functions */
    public function listUser()
    {
        $users = User::whereHas('roles', function($q)
            {
                $q->where('name', '=', 'User');
            })->orderBy('id', 'desc')->paginate(10);
        $this->layout->title = 'User listings';
        $this->layout->main = View::make('dash')->nest('content', 'users.list', compact('users'));
    }

    /**
     * Delete user
     *
     * @param User $user
     * @return mixed
     */
    public function deleteUser(User $user)
    {
        $user->delete();
        return Redirect::route('user.list')->with('success', 'User is deleted!');
    }

    /**
     * Random assign kandy accounts for users
     *
     * @return mixed
     */
    public function assignkandyuser()
    {
        KandyLaravel::assignAllUser();
        return Redirect::route('user.list');
    }

    /**
     * Update kandy user
     *
     * @return mixed
     */
    public function updatekandyuser()
    {
        if (empty($_POST['pk'])) {
            return Response::make('Your request is invalid.', 403);
        }
        $main_user_id = $_POST['pk'];
        $kandy_user_id = isset($_POST['value']) ? $_POST['value'] : null;
        if ($kandy_user_id === "") {
            return Response::make('Updated successfully.', 200);
        }

        $model = User::findOrFail($main_user_id);
        // Update
        $result = KandyLaravel::assignUser($model->id, $kandy_user_id);
        if (!$result['success']) {
            // No more kandy user available, create a new one
            $result = KandyLaravel::createUser($model->username, $model->email, $model->id);
            if ($result['success']) {
                // Create and assign successfully
                return Response::json(array_merge($result, array('user' => $model->username)), 200);
            } else {
                // Fail to create new Kandy user
                return Response::make("There is no more available Kandy user. " . $result['message'], 500);
            }
        } else {
            // Update successfully
            return Response::json($result, 200);
        }
    }

    /**
     * Unassign kandy user
     *
     * @return mixed
     */
    public function unassignkandyuser()
    {
        if (empty($_POST['pk'])) {
            return Response::make('Your request is invalid.', 403);
        }

        $main_user_id = $_POST['pk'];
        $model = User::find($main_user_id);
        if (empty($model)) {
            return Response::make('Your request is invalid.', 403);
        }

        $result = KandyLaravel::unassignUser($model->id);
        if (!$result) {
            return Response::make('Sorry! There was an error with your request. Please try again later.', 500);
        } else {
            return Response::make('Unassign successfully.', 200);
        }
    }

    /**
     * Synchronization Kandy user
     *
     */
    public function synckandyuser()
    {
        $response = KandyLaravel::syncUsers();
        $statusCode = $response['success'] ? 200 : 500;
        return Response::make($response['message'], $statusCode);
    }

    /**
     * List Kandy user
     *
     */
    public function listKandyuser()
    {
        $q = isset($_GET['query']) ? $_GET['query'] : null;
        $kandyUserOptions = User::getKandyUsersOptions($q);
        echo json_encode($kandyUserOptions);
        exit();
    }

    /**
     * listing agents action
     */
    public function listAgents()
    {
        $kandyUserTable = \Config::get('kandy-laravel::kandy_user_table');
        $mainUserTable = \Config::get('kandy-laravel::user_table');
        $agentType = \Kodeplusdev\Kandylaravel\Kandylaravel::USER_TYPE_CHAT_AGENT;
        $rateTable = \Config::get('kandy-laravel::kandy_live_chat_rate_table');

        $users = User::whereHas('roles', function($q)
        {
            $q->where('name', '=', 'User');
        })->orderBy("$mainUserTable.id", 'desc')
            ->join($kandyUserTable, "$mainUserTable.id" , '=', "$kandyUserTable.main_user_id")
            ->leftJoin($rateTable, "$mainUserTable.id", '=', "$rateTable.main_user_id")
            ->select("$kandyUserTable.id as id", "$mainUserTable.email","username",
                "user_id as kandy_user", "$kandyUserTable.main_user_id", \DB::raw("avg($rateTable.point) as average"))
            ->groupBy("$mainUserTable.id")
            ->where("$kandyUserTable.type",'=',$agentType)
            ->orderBy("average", "DESC")
            ->paginate(10);
        $this->layout->title = 'Chat Agent listings';
        $this->layout->main = View::make('dash')->nest('content', 'users.agentlist', compact('users'));

    }

    /**
     * Add agent action
     * @return mixed
     */
    public function addChatAgent ()
    {
        $userId = \Request::get('id','');
        $user = \Kodeplusdev\Kandylaravel\KandyUsers::find($userId);
        if($user){
            $user->type = \Kodeplusdev\Kandylaravel\Kandylaravel::USER_TYPE_CHAT_AGENT;
            if($user->save()){
                $result = array(
                    'success'   => true,
                    'user'      => $user,
                );
            }else{
                $result = array(
                    'success'   => false,
                    'message'   => 'Cannot add user'
                );
            }
        }else{
            $result = array(
                'success'   => false,
                'message'   => 'user not found'
            );
        }
        return \Response::json($result, 200);
    }

    /**
     * Remove agent action
     * @return mixed
     */
    public function removeChatAgent()
    {
        $userId = \Request::get('id','');
        $user = \Kodeplusdev\Kandylaravel\KandyUsers::find($userId);
        if($user){
            //change type of user to normal
            $user->type = \Kodeplusdev\Kandylaravel\Kandylaravel::USER_TYPE_NORMAL;
            if($user->save()){
                return \Redirect::back();
            }else{
                $error = array(
                    'message'   => 'Could not remove agent'
                );
                return \Redirect::back()->withErrors($error);
            }

        }else{
            $error = array(
                'message'   => 'user not found'
            );
            return \Redirect::back()->withErrors($error);
        }
    }

    public function viewAgent()
    {
        $tableUser = \Config::get('kandy-laravel::user_table');
        $tableKandyUser = \Config::get('kandy-laravel::kandy_user_table');
        $agentId = \Request::get('agentId', 0);
        $agent = \DB::table($tableUser)
            ->join($tableKandyUser, "$tableUser.id", '=', "$tableKandyUser.main_user_id")
            ->select('username', \DB::raw("$tableKandyUser.user_id as kandy_user"))
            ->where("$tableUser.id", "=", $agentId)->first();
        $agentRates = \Kodeplusdev\Kandylaravel\KandyLiveChatRate::where('main_user_id', '=', $agentId)
            ->orderBy("rated_time", "DESC")->orderBy("point",'DESC')->paginate(20);
        if(!$agent){
            return \Response::make('Not found', 404);
        }else{
            $this->layout->title = "View agent rates";
            $this->layout->main = View::make("dash")->nest('content', 'users.viewAgent', compact('agent', 'agentRates'));
        }

    }


}
