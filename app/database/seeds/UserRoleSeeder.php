<?php

class UserRoleSeeder extends Seeder {

	public function run()
	{
        // Delete any data
        DB::table('role_user')->delete();
        DB::table('roles')->delete();
        DB::table('users')->delete();

        // Insert roles
        $adminRoleId = DB::table('roles')->insertGetId(
            array(
                'name' => "Super Admin",
                'level' => 10,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            )
        );
        $userRoleId = DB::table('roles')->insertGetId(
            array(
                'name' => "User",
                'level' => 5,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            )
        );

        // Insert admin
        $admin = new User();
        $admin->username = "admin";
        $admin->email = "admin@gmail.com";
        $admin->password = "111111";
        $admin->verified = 1;
        $admin->save();
        $admin->roles()->sync(array($adminRoleId));

        // Insert user
        /*$user = new User();
        $user->username = "demo";
        $user->email = "demo@gmail.com";
        $user->password = "111111";
        $user->verified = 1;
        $user->save();
        $user->roles()->sync(array($userRoleId));*/
	}
}
