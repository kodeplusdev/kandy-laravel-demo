<?php

class Msg
{	
	public static function key()
	{
		return 'message';
	}
	
	public static function downloadLog($message)
	{
		$message = date('Y-m-d H:i:s')." - {$message}".PHP_EOL;
        $fileName = base_path() . 'storage/logs/downloads-' . date('Y-m-d') . '.log';
		@File::append($fileName, $message);
	}
	
	public static function info($msgs, $heading = "Info")
	{
		return self::message($msgs, $heading);
	}
	
	public static function error($msgs, $heading = "Error")
	{
		return self::message($msgs, "Error!", "error");
	}
	
	public static function message($msgs, $heading="Info", $class="info")
	{
		$data = array ('class' => $class, 'heading' => $heading);
		$messages = array();
		
		if(is_array($msgs)) $messages = array_merge($messages, $msgs);
		else $messages[] = $msgs;
		
		$data['messages'] = $messages;
		
		return View::make(self::key(), $data)->render();
	}
	
	public static function lognmail($msg, $level = 'info', $heading = "System Message", $msgData = null)
	{
		Log::write($level, $msg);
		
		/*Message::send(function($email) use ($msg, $level, $heading, $msgData)
		{
			$email->to(Config::get('lco.tech-support-email'));
			$email->from(Config::get('lco.app-email-from'), Config::get('lco.app-from'));
		
			$email->subject($heading);
			$email->body('view: emails.generic');
		
			$email->body->heading = $heading;
			$email->body->message = nl2br( $msg );
			if($msgData != null) $email->body->msgData = $msgData;
		
			$email->html(true);
		});*/

        Mail::send(
            'emails.generic',
            array('content' => nl2br( $msg ), 'heading' => $heading, 'msgData' => $msgData),
            function ($email) use ($heading) {
                $email->to(Config::get('lco.tech-support-email'));
                $email->from(Config::get('lco.app-email-from'), Config::get('lco.app-from'));
                $email->subject($heading);
            }
        );
	}
}

?>
