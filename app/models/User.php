<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Toddish\Verify\Models\User as VerifyUser;

class User extends VerifyUser implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    protected $fillable = ['username', 'email'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{

		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
	
	/**
	 * Required for laravel 4.1.26+
	 *
	 */
	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

    public function createUser()
    {
        $this->save();

        // Assign the Role to the User
        $userRoleId = getRoleUserId();
        $this->roles()->sync(array($userRoleId));

        // Create Kandy user
        /*$result = KandyLaravel::createUser($this->username, $this->email, $this->id);

        // @see: Catch error on application side
        if ($result['success'] === true) {
            // Do success
        } else {
            // Do error
            $errorMessage = $result['message'];
        }*/
    }

    /**
     * Assign attributes for
     */
    public function assignAttributes()
    {
        $this->username = Input::get("username");
        $this->email = Input::get("email");
        $this->password = Input::get("password");
        $this->verified = 1;
    }

    /**
     * Get kandy user to display in grid view
     *
     * @return mixed
     */
    public function getKandyUser()
    {
        $kandyUser = KandyLaravel::getUser($this->id);
        $class = 'kandy-editable editable editable-click';
        if (empty($kandyUser)) {
            $class .= " editable-empty";
            $kandyUserId = "";
        } else {
            $kandyUserId = $kandyUser->user_id;
        }
        $result = HTML::link(
            "#",
            $kandyUserId,
            array(
                'class' => $class,
                'data-type' => 'select2',
                'data-pk' => $this->id,
                'data-url' => URL::action('UserController@updatekandyuser'),
                'data-title' => "Kandy User",
                'data-emptytext' => "not selected"
            )
        );
        $button = '<button type="button" style="height: 25px;width: 30px;margin:0 0 0 10px;"
                        title="Unassign"
                        class="custom-cancel-button custom-button ui-button ui-state-default ui-corner-all ui-button-icon-only"
                        data-id=' . $this->id . '>
                            <span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>
                            <span class="ui-button-text">cancel</span>
                    </button>
                    <button type="button" style="height: 25px;width: 30px;margin:0 0 0 10px;"
                        title="Random Assign"
                        class="custom-add-button custom-button ui-button ui-state-default ui-corner-all ui-button-icon-only"
                        data-id=' . $this->id . '>
                            <span class="ui-button-icon-primary ui-icon ui-icon-plusthick"></span>
                            <span class="ui-button-text">cancel</span>
                    </button>';
        $result .= $button;
        return $result;
    }

    /**
     * Get kandy user options
     *
     * @param $q
     * @return array
     */
    public static function getKandyUsersOptions($q)
    {
        $result = array();
        $kandyUsers = KandyLaravel::listUser(Kodeplusdev\Kandylaravel\Kandylaravel::KANDY_USER_UNASSIGNED);
        $i = 0;
        foreach ($kandyUsers as $kandyUser) {
            if (empty($q)) {
                if ($i >= 5) {
                    break;
                }
                $option = array('id' => $kandyUser->user_id, 'text' => $kandyUser->user_id);
                array_push($result, $option);
                ++$i;
            } else {
                if (strpos($kandyUser->user_id, $q) !== false) {
                    $option = array('id' => $kandyUser->user_id, 'text' => $kandyUser->user_id);
                    array_push($result, $option);
                }
            }
        }
        return $result;
    }

}
