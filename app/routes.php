<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Model Bindings */
Route::model('post', 'Post');
Route::model('comment', 'Comment');

/* User routes */
Route::get('/post/{post}/show', ['as' => 'post.show', 'uses' => 'PostController@showPost']);
Route::post('/post/{post}/comment', ['as' => 'comment.new', 'uses' => 'CommentController@newComment']);

/* Admin routes */
Route::group(
    ['prefix' => 'admin', 'before' => 'auth'],
    function () {
        /*get routes*/
        Route::get(
            'dash-board',
            function () {
                $layout = View::make('master');
                $layout->title = 'DashBoard';
                $username = Auth::user()->username;
                $layout->main = View::make('dash')->with('content', "Hi $username, Welcome to Dashboard!");
                return $layout;

            }
        );
        Route::get('/post/list', ['as' => 'post.list', 'uses' => 'PostController@listPost']);
        Route::get('/post/new', ['as' => 'post.new', 'uses' => 'PostController@newPost']);
        Route::get('/post/{post}/edit', ['as' => 'post.edit', 'uses' => 'PostController@editPost']);
        Route::get('/post/{post}/delete', ['as' => 'post.delete', 'uses' => 'PostController@deletePost']);
        Route::get('/comment/list', ['as' => 'comment.list', 'uses' => 'CommentController@listComment']);
        Route::get('/comment/{comment}/show', ['as' => 'comment.show', 'uses' => 'CommentController@showComment']);
        Route::get(
            '/comment/{comment}/delete',
            ['as' => 'comment.delete', 'uses' => 'CommentController@deleteComment']
        );
        Route::get('/user/list', ['as' => 'user.list', 'uses' => 'UserController@listUser']);
        Route::get('/user/{user}/delete', ['as' => 'user.delete', 'uses' => 'UserController@deleteUser']);
        Route::get('/user/{user}/delete', ['as' => 'user.delete', 'uses' => 'UserController@deleteUser']);
        Route::get('/user/listkandy', ['as' => 'user.listkandy', 'uses' => 'UserController@listKandyuser']);
        Route::get('/user/synckandyuser', ['as' => 'user.synckandyuser', 'uses' => 'UserController@synckandyuser']);
        Route::get('/user/assignkandyuser', ['uses' => 'UserController@assignkandyuser']);
        Route::post('/user/updatekandyuser', ['uses' => 'UserController@updatekandyuser']);
        Route::post('/user/unassignkandyuser', ['uses' => 'UserController@unassignkandyuser']);
        Route::get('/user/listagent', ['as' => 'user.listagent', 'uses' => 'UserController@listAgents']);
        Route::get('/user/addchatagent', ['as' => 'user.addchatagent', 'uses' => 'UserController@addChatAgent']);
        Route::get('/user/removechatagent', ['as' => 'user.removechatagent', 'uses' => 'UserController@removeChatAgent']);
        Route::get('/user/viewAgent', ['as' => 'user.viewagent', 'uses' => 'UserController@viewAgent']);


        /*post routes*/
        Route::post('/post/save', ['as' => 'post.save', 'uses' => 'PostController@savePost']);
        Route::post('/post/{post}/update', ['as' => 'post.update', 'uses' => 'PostController@updatePost']);
        Route::post(
            '/comment/{comment}/update',
            ['as' => 'comment.update', 'uses' => 'CommentController@updateComment']
        );

    }
);

Route::group(['prefix' => 'phone', 'before' => 'auth'], function () {
        Route::get('/videoAnswer', 'PhoneController@videoAnswer');
        Route::get('/voiceCall', 'PhoneController@voiceCall');
        Route::get('/presenceList', 'PhoneController@presenceList');
        Route::get('/chat', 'PhoneController@chat');
        Route::get('/groupChat', 'PhoneController@groupChat');
    });

Route::get('/coBrowsing', array('before' => 'auth', 'uses' => 'CollaborationController@coBrowsing'));
/* Home routes */
Route::controller('/', 'BlogController');

/* View Composer */
View::composer('sidebar', function ($view) {
    $view->recentPosts = Post::orderBy('id', 'desc')->take(5)->get();
});

