{{KandyLaravel::init($userId)}}
<div>
    <h2>Co-browsing Demo</h2>
{{KandyCoBrowsing::show(array(
    'holderId'                  => 'cobrowsing-holder',
    'btnTerminateId'            => 'btnTerminateSession',
    'btnStopId'                 => 'btnStopCoBrowsing',
    'btnLeaveId'                => 'btnLeaveSession',
    'btnStartBrowsingViewerId'  => 'btnStartCoBrowsingViewer',
    'btnStartCoBrowsingId'      => 'btnStartCoBrowsing',
    'btnConnectSessionId'       => 'btnConnectSession',
    'currentUser'               => KandyLaravel::getUser($userId),
    'sessionListId'             => 'openSessions'
))}}
</div>
<div id="sessionModal" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
      <h2 id="modalTitle">Create new Session</h2>
        <div class="row">
            <div class="small-9 columns">
              <div class="row">
                <div class="small-4 columns">
                  <label for="right-label" class="right inline">Session name</label>
                </div>
                <div class="small-8 columns">
                  <input type="text" id="create_session_name" placeholder="Session name">
                  <p class="errors"></p>
                </div>
              </div>
              <div class="row">
                <div class="small-5 columns small-offset-4">
                    <a href="javascript:;" id="btnCreateSession" class="button tiny">Create</a>
                </div>
              </div>

            </div>
          </div>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<script>
$(function(){
    $("#btnCreateSession").click(function(){
    var sessionName = $('#create_session_name').val();
    var errors = [];
    var creationTime = new Date().getTime();
    var timeExpire = creationTime + 31536000;// expire in 1 year
    var errorContainer = $(".errors");
    errorContainer.empty();
    if(sessionName == ''){
        errors.push('Session must have a name.');
    }
    if(errors.length){
        errorContainer.html(errors.join('<br>'));
        errorContainer.show();
        return;
    }
    var sessionCreateConfig = {
        session_type: 'cobrowsing',
        session_name: sessionName,
        creation_timestamp: creationTime,
        expiry_timestamp: timeExpire
    };
    kandy_createSession(sessionCreateConfig, function(){
        kandy_getOpenSessionsByType('cobrowsing', loadSessionList);
        $('#sessionModal').foundation('reveal', 'close');
    });
});
})
</script>
