@if(!$kandyUser)
    <h3>You are not assigned to any kandy user. Please contact your administrator</h3>
@else
{{KandyLaravel::init($userId);}}
{{ HTML::style('assets/css/KandyQsChat.css') }}
{{ HTML::script('/assets/js/kandy/KandyQsChat.js') }}
<div>
    <h2>Sample Chat</h2>

    <div id="loading"><h2>Loading Kandy Components ...</h2></div>
    <div id="chat-wrapper" style="display: none">
        {{KandyChat::show(
            array(
                "id"      => "myChat",
                "class"   => "myChatStyle",
                "options" => array(
                    "contact"   => array(
                        "id"    => "myContact",
                        "label" => "Contacts",
                    ),
                    "message"   => array(
                        "class"    => "myMessage",
                        "label" => "Messages",
                    ),
                    "user"      => array(
                        "name"  => $kandyUser->user_id,
                        'kandyUser' => $kandyUser->user_id . '@' . $kandyUser->domain_name
                    )
                )

            )
        )
        }}
    </div>
    <div id="myModal" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <h3 id="modalTitle">Create new group</h3><hr>
            <div class="row">
                <div class="small-9 columns">
                  <div class="row">
                    <div class="small-4 columns">
                      <label for="right-label" class="right inline">Group name</label>
                    </div>
                    <div class="small-8 columns">
                      <input type="text" id="create_session_name" placeholder="Group name">
                    </div>
                  </div>
                  <div class="row">
                    <div class="small-5 columns small-offset-4">
                        <a href="javascript:;" id="btnCreateGroup" class="button tiny">Save</a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="errors small-12 columns">

                    </div>
                  </div>
                </div>
              </div>
          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
    <div id="inviteModal" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
      <h3 id="modalTitle">Add user to group</h3><hr>
        <div class="row">
            <div class="small-9 columns">
              <div class="row">
                <div class="small-4 columns">
                  <label for="right-label" class="right inline">Username</label>
                </div>
                <div class="small-8 columns">
                  <input class="select2" id="invite_username" placeholder="Username">
                </div>
              </div>
              <div class="row">
                <div class="small-5 columns small-offset-4">
                    <a href="javascript:;" id="btnInviteUser" class="button tiny">Add</a>
                </div>
              </div>
              <div class="row">
                <div class="errors small-12 columns">
                </div>
              </div>
            </div>
          </div>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

</div>

<script>
    $(function(){
        $('#btnCreateGroup').click(function(){
            var groupName = $('#create_session_name').val();
            var errors = [];
            var errorContainer = $(".errors");
            errorContainer.empty();
            if(groupName == ''){
                errors.push('Group must have a name.');
            }
            if(errors.length){
                errorContainer.html(errors.join('<br>'));
                errorContainer.show();
                return;
            }
            kandy_createGroup(groupName, kandy_loadGroups);
            $('#create_session_name').val('');
            $('#myModal').foundation('reveal', 'close');
        });

        $('#btnInviteUser').click(function(){
            var username = $('#invite_username').val();
            var groupId = $("#inviteModal").data('group');
            var errors = [];
            var errorContainer = $(".errors");
            errorContainer.empty();
            if(!username){
                errors.push('Please provide a username.');
            }
            if(!groupId){
                errors.push('Cannot specify group to add user');
            }
            if(errors.length){
                errorContainer.html(errors.join('<br>'));
                errorContainer.show();
                return;
            }
            kandy_inviteUserToGroup(groupId, [username]);
            $('#inviteModal').foundation('reveal', 'close');
        })
    });


</script>
@endif



