<?php // Initialize Kandy setting ?>
{{KandyLaravel::init($userId);}}
{{ HTML::style('assets/css/KandyQsPresence.css') }}
{{ HTML::script('/assets/js/kandy/KandyQsPresence.js') }}
<div>
    <h2>User Presence</h2>
    <h4>This sample application demonstrates the Kandy code for viewing other users' status.</h4>

    <div id="topRow">
        {{KandyStatus::show(
        array(
            "title" => "My Status",
            "id"    => "presence",
            "class" => "myStatusStyle",
        ))}}

    </div><!--endD topRow -->

    <div id="contact-wrapper">
        {{KandyAddressBook::show(
        array(
            "title" => "My Contact",
            "id"    => "contactsAndDirSearch",
            "class" => "myAddressBookStyle",
        ))}}
    </div>

</div>

