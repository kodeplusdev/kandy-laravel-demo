SETUP KANDY-LARAVEL PACKAGE (for DEV)
1. Push code to version control (github or bitbuckage or svn)
2. Push code to https://packagist.org
    - Create account on https://packagist.org
    - Submit package to https://packagist.org
        + Go to: https://packagist.org => Click "Submit package" button => Enter Repository URL and click CHECK
    - Set auto-update for package
        (from packagist.org) Enabling the Packagist service hook ensures that your package will always be updated instantly when you push to GitHub.
        To do so you can go to your GitHub repository, click the "Settings" button, then "Webhooks & Services".
        Add a "Packagist" service, and configure it with your API token (see above), plus your Packagist username.
        Check the "Active" box and submit the form.
        You can then hit the "Test Service" button to trigger it and check if Packagist removes the warning about the package not being auto-updated.


